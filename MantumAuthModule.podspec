Pod::Spec.new do |spec|

  spec.name         = "MantumAuthModule"
  spec.version      = "0.1.0"
  spec.summary      = "A CocoaPods library written in Swift"

  spec.description  = "Help mantum tu use some interfaces, that helps with the structure of iOS apps."
  
  spec.homepage     = "https://bitbucket.org/3astronautas/mantum-auth-module"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Tres Astronautas" => "juanes@tresastronautas.com" }

  spec.ios.deployment_target = "12.1"
  spec.swift_version = "4.2"
  spec.framework = "UIKit", "SwiftRealm" 
  spec.source        = { :git => "https://bitbucket.org/3astronautas/mantum-auth-module", :tag => "#{spec.version}" }
  spec.source_files  = "MantumAuthModule/**/*.{h,m,swift}"

end

