//
//  MantumAuthModule.swift
//  MantumAuthModule
//
//  Created by juan esteban  chaparro machete on 14/11/19.
//  Copyright © 2019 Tres Astronautas. All rights reserved.
//

import Foundation
import RealmSwift

public class MantumAuth: NSObject {
    
    public var authService: AuthService
    
    public init(baseUrl: URL , appDelegate:Any, realm: Realm) {
        self.authService = AuthService(baseUrl: baseUrl, appDelegate: appDelegate, realm: realm)
    }
}
