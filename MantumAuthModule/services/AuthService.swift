//
//  AuthService.swift
//  MantumAuthModule
//
//  Created by juan esteban  chaparro machete on 14/11/19.
//  Copyright © 2019 Tres Astronautas. All rights reserved.
//

import Foundation
import RealmSwift
public class AuthService: Services {
    
    
    private let url: URL
    private let appDelegate: AppDelegate
    private let realm: Realm
    
    public init(baseUrl: URL, appDelegate: Any, realm :Realm ) {
        self.url = baseUrl
        self.appDelegate = appDelegate as! AppDelegate
        self.realm = realm
        super.init()
        
    }
    func actual() -> Cuenta? {
        return realm.objects(Cuenta.self)
            .filter("autenticado = true")
            .first
    }
    
    func cerrarSesion() -> Void {
    
        try! realm.write {
            let resultado = realm.objects(Cuenta.self)
                .filter("autenticado = true")
            
            resultado.forEach { (cuenta) in
                cuenta.autenticado = false
            }
        }
    }
    
    func registrar(cuenta: Cuenta, callback: @escaping (Bool, String, Int64) -> ()) -> Void {
        let json = cuenta.toJson()
        if json == nil {
            callback(true, "No fue posible realizar el registro, intente de nuevo por favor", 0)
            return
        }
        
        let url = self.url
        let profile = Profile(url: "\(url)/v\(self.getVersion())/usuario/registrar/\(json!)")
        
        self.get(profile: profile) { (response) in
            if response.error {
                callback(true, response.message, 0)
                return
            }
            
        
            if let body = response.body["oData"] as? [String: Any] {
                var registro = false
                if let bRegistro = body["bRegistro"] as? Bool {
                    registro = bRegistro
                }
                
                var mensaje = "No fue posible realizar el registro, intente de nuevo por favor"
                if let sMensaje = body["sMensaje"] as? String {
                    mensaje = sMensaje
                }
                
                if !registro {
                    callback(true, mensaje, 0)
                    return
                }
                
                let cuenta = Cuenta.fromJson(json: body)
                try! self.realm.write {
                    self.realm.create(Cuenta.self, value: cuenta,update:.all)
                    NSLog("Guardar cuenta -> \(cuenta.correoElectronico)")
                }
                
                callback(false, mensaje, cuenta.id)
            }
        }
    }
    
    func actualizar(cuenta: Cuenta, callback: @escaping (Bool, String) -> ()) -> Void {
        let json = cuenta.toJson()
        if json == nil {
            callback(true, "No fue posible realizar el registro, intente de nuevo por favor")
            return
        }
        
         let url = self.url
        let profile = Profile(url: "\(url)/v\(self.getVersion())/usuario/actualizar/\(cuenta.id)/iOS/\(json!)")
        
        self.get(profile: profile) { (response) in
            if response.error {
                callback(true, response.message)
                return
            }
            
            if let body = response.body["oData"] as? [String: Any] {
                var registro = false
                if let bRegistro = body["bRegistro"] as? Bool {
                    registro = bRegistro
                }
                
                var mensaje = "No fue posible actualizar el usuario, intente de nuevo por favor"
                if let sMensaje = body["sMensaje"] as? String {
                    mensaje = sMensaje
                }
                
                if !registro {
                    callback(true, mensaje)
                    return
                }
                
                let cuenta = Cuenta.fromJson(json: body)
                let cuentaActual = self.actual()
                cuenta.id = (cuentaActual?.id)!
                cuenta.autenticado = (cuentaActual?.autenticado)!
                cuenta.contactable = (cuentaActual?.contactable)!
                try! self.realm.write {
                    self.realm.create(Cuenta.self, value: cuenta, update: .all)
                    NSLog("Guardar cuenta -> \(cuenta.correoElectronico)")
                }
                
                callback(false, mensaje)
            }
        }
    }
    
    func obtener(cuenta: Cuenta, callback: @escaping (Cuenta) -> ()) -> Void {
         let url = self.url
        let profile = Profile(url: URL(string: "\(url)/v\(self.getVersion())/usuario/buscar/\(cuenta.tipoDocumento)/\(cuenta.documento)/\(cuenta.correoElectronico)")!)
        
        self.get(profile: profile) { (response) in
            if let body = response.body["oData"] as? [String: Any] {
                let cuenta = Cuenta.fromJson(json: body)
                callback(cuenta)
            }
        }
    }
    
    func autenticar(usuario: String, contrasena: String,callback: @escaping (Bool, Bool, String) -> ()) -> Void {
         let url = self.url
        let profile = Profile(url: URL(string: "\(url)/v\(self.getVersion())/usuario/iniciarSesion/\(contrasena)/\(usuario)/\(self.appDelegate.getTokenForPush() ?? "1" )iOS")!)
        
        self.get(profile: profile) { (response) in
            if response.error {
                callback(response.error, false, response.message)
                return
            }
            
            if let body = response.body["oData"] as? [String: Any] {
                let cuenta = Cuenta.fromJson(json: body)
                if !cuenta.autenticado {
                    callback(false, false, "No fue posible iniciar sesión")
                    return
                }
                
                try! self.realm.write {
                    let cuentas = self.realm.objects(Cuenta.self).filter("autenticado = true")
                    cuentas.forEach({ (cuenta) in
                        cuenta.autenticado = false
                    })
                    
                    self.realm.create(Cuenta.self, value: cuenta, update: .all)
                    NSLog("Guardar cuenta -> \(cuenta.correoElectronico)")
                }
                
                callback(false, true, "Se ha iniciado sesión satisfactoriamente")
            }
        }
    }
    
    func autenticarFacebook(idFacebook: String, callback: @escaping (Bool, Bool, String) -> ()) -> Void {
         let url = self.url
        let profile = Profile(url: URL(string: "\(url)/v\(self.getVersion())/usuario/iniciarSesionFacebook/\(idFacebook)/\(self.appDelegate.getTokenForPush() ?? "1")iOS")!)
        
        self.get(profile: profile) { (response) in
            if response.error {
                callback(response.error, false, response.message)
                return
            }
            
            if let body = response.body["oData"] as? [String: Any] {
                let cuenta = Cuenta.fromJson(json: body)
                if !cuenta.autenticado {
                    callback(false, false, "No fue posible iniciar sesión")
                    return
                }
                
                try! self.realm.write {
                    let cuentas = self.realm.objects(Cuenta.self).filter("autenticado = true")
                    cuentas.forEach({ (cuenta) in
                        cuenta.autenticado = false
                    })
                    
                    self.realm.create(Cuenta.self, value: cuenta, update: .all)
                    NSLog("Guardar cuenta -> \(cuenta.correoElectronico)")
                }
                
                callback(false, true, "Se ha iniciado sesión satisfactoriamente")
            }
        }
    }
    
    func actualizarFacebook(idCuenta: Int64, idFacebook: String, callback: @escaping (Bool, String) -> ()) -> Void {
        
         let url = self.url
        let profile = Profile(url: "\(url)/v\(self.getVersion())/usuario/actualizarFacebook/\(idCuenta)/\(idFacebook)")
        
        self.get(profile: profile) { (response) in
            if response.error {
                callback(true, response.message)
                return
            }
            
            if let body = response.body["oData"] as? [String: Any] {
                var registro = false
                if let bRegistro = body["bExito"] as? Bool {
                    registro = bRegistro
                }
                
                var mensaje = "No fue posible actualizar el facebook del usuario, intente de nuevo por favor"
                if let sMensaje = body["sMensaje"] as? String {
                    mensaje = sMensaje
                }
                
                if !registro {
                    callback(true, mensaje)
                    return
                }
                
                callback(false, mensaje)
            }
        }
    }
    
}
