//
//  Cuenta.swift
//  MantumAuthentication
//
//  Created by juan esteban  chaparro machete on 14/11/19.
//  Copyright © 2019 Tres Astronautas. All rights reserved.
//

import Foundation
import RealmSwift
class Cuenta: Object {
    
    @objc dynamic var id: Int64 = 0
    @objc dynamic var tipoDocumento: Int = 0
    @objc dynamic var documento: String = ""
    @objc dynamic var correoElectronico: String = ""
    @objc dynamic var nombre: String = ""
    @objc dynamic var apellido: String = ""
    @objc dynamic var fechaNacimiento: String = ""
    @objc dynamic var telefono: String = ""
    @objc dynamic var genero: String = ""
    @objc dynamic var estadoCivil: String = ""
    @objc dynamic var autenticado: Bool = false
    @objc dynamic var contactable: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func toDictionary() -> [String : Any] {
        return [
            "oData" : [
                "iTipoDocumento" : self.tipoDocumento,
                "sDocumento" : self.documento,
                "sNombre": self.nombre,
                "sApellido" : self.apellido,
                "sCorreoElectronico" : self.correoElectronico,
                "sFechaNacimiento" : self.fechaNacimiento,
                "sTelefono" : self.telefono,
                "sGenero" : self.genero,
                "sEstadoCivil" : self.estadoCivil,
                "contactable" : self.contactable
            ]
        ]
    }
    
    func toJson() -> String? {
        let data = try! JSONSerialization.data(withJSONObject: toDictionary(), options: [])
        return String(data: data, encoding: .utf8) ?? nil
    }
    
    static func fromJson(json: [String: Any]) -> Cuenta {
        let cuenta = Cuenta()
        if let id = json["iIdUsuario"] as? Int64 {
            cuenta.id = id
        }
        
        if let autenticado = json["bLogin"] as? Bool {
            cuenta.autenticado = autenticado
        }
        
        if let usuario = json["oUsuario"] as? [String:Any] {
            
            if let tipoDocumento = usuario["iTipoDocumento"] as? Int {
                cuenta.tipoDocumento = tipoDocumento
            }
            
            if let documento = usuario["sDocumento"] as? String {
                cuenta.documento = documento
            }
            
            if let correoElectronico = usuario["sCorreoElectronico"] as? String {
                cuenta.correoElectronico = correoElectronico
            }
            
            if let nombre = usuario["sNombre"] as? String {
                cuenta.nombre = nombre
            }
            
            if let apellido = usuario["sApellido"] as? String {
                cuenta.apellido = apellido
            }
            
            if let fechaNacimiento = usuario["sFechaNacimiento"] as? String {
                cuenta.fechaNacimiento = fechaNacimiento
            }
            
            if let telefono = usuario["sTelefono"] as? String {
                cuenta.telefono = telefono
            }
            
            if let genero = usuario["sGenero"] as? String {
                cuenta.genero = genero
            }
            
            if let estadoCivil = usuario["sEstadoCivil"] as? String {
                cuenta.estadoCivil = estadoCivil
            }
            
            if let contactable = usuario["iContactable"] as? Bool {
                cuenta.contactable = contactable
            }
        }
        
        return cuenta;
    }
}


protocol AppDelegate {
    func getTokenForPush() -> String?
    
}
